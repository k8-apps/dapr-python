===========
dapr-python
===========


.. image:: https://img.shields.io/pypi/v/dapr_python.svg
        :target: https://pypi.python.org/pypi/dapr_python

.. image:: https://img.shields.io/travis/emctl/dapr_python.svg
        :target: https://travis-ci.com/emctl/dapr_python

.. image:: https://readthedocs.org/projects/dapr-python/badge/?version=latest
        :target: https://dapr-python.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status


.. image:: https://pyup.io/repos/github/emctl/dapr_python/shield.svg
     :target: https://pyup.io/repos/github/emctl/dapr_python/
     :alt: Updates



distributed calculator for the dapr framework for python


* Free software: MIT license
* Documentation: https://dapr-python.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
